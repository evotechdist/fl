// var signupUser = function(name, password) {
// var urlSignup = "http://myfl.esy.es/insertdata/signup.php?format=json&name=" + name + "&password=" + password + "&image=" + photo;
// var signup = Ti.Network.createHTTPClient({
// onload : function() {
// var result = {
// content : JSON.parse(this.responseText),
// success : true
// };
// callback(result);
// },
// onerror : function() {
// var result = {
// err : error,
// success : false
// };
// callback(result);
// }
// });
// login.open("GET", urlSignup);
// login.send();
// };

var viewProfile = function(iduser, callback) {
    var urlProfile = "http://myfl.esy.es/viewProfile.php?format=json&iduser=" + iduser;
    var view = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                content : JSON.parse(this.responseText),
                success : true
            };
            callback(result);
        },
        onerror : function(error) {
            var result = {
                err : error,
                success : false
            };
            callback(result);
        }
    });
    view.open("GET", urlProfile);
    view.send();
};

var updateLocation = function(iduser, address, date, latitude, longitude, callback) {
    var urlUpdateLocation = "http://myfl.esy.es/updateLocation.php?format=json&iduser=" + iduser + "&address=" + address + "&date=" + date + "&latitude=" + latitude + "&longitude=" + longitude;
    var update = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                content : JSON.parse(this.responseText),
                success : true
            };
            callback(result);

        },
        onerror : function(error) {
            var result = {
                err : error,
                success : false
            };
            callback(result);
        }
    });
    update.open("GET", urlUpdateLocation);
    update.send();
};

var viewFriend = function(name, callback) {
    var urlView = "http://myfl.esy.es/viewFriend.php?format=json&name=" + name;
    var view = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                content : JSON.parse(this.responseText),
                success : true
            };
            callback(result);
        },
        onerror : function(error) {
            var result = {
                err : error,
                success : false
            };
            callback(result);
        }
    });
    view.open("GET", urlView);
    view.send();
};

var findUser = function(txtSearch, callback) {
    var urlFind = "http://myfl.esy.es/findFriend.php?format=json&name=" + txtSearch;
    var find = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                content : JSON.parse(this.responseText),
                success : true
            };
            callback(result);
        },
        onerror : function(error) {
            var result = {
                err : error,
                success : false
            };
            callback(result);
        }
    });
    find.open("GET", urlFind);
    find.send();
};

var addUser = function(txtSearch, name, callback) {
    var urlAdd = "http://myfl.esy.es/addFriend.php";
    // ?format=json&userRequest='" + txtSearch + "'
    // &name='" + name + "'";
    var add = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                success : true
            };
            callback(result);
        },
        onerror : function(error) {
            var result = {
                success : false
            };
            callback(result);
        }
    });
    add.open("POST", urlAdd);
    add.send({
        userRequest : "'"+txtSearch+"'",
        name : "'"+name+"'",
    });
};
 
var loginUser = function(name, password, callback) {
    var urlLogin = "http://myfl.esy.es/login.php?format=json&name=" + name + "&pass=" + password;
    var login = Ti.Network.createHTTPClient({
        onload : function() {
            var result = {
                content : JSON.parse(this.responseText),
                success : true
            };
            callback(result);
        },
        onerror : function(error) {
            var result = {
                err : error,
                success : false
            };
            callback(result);
        }
    });
    login.open("GET", urlLogin);
    login.send();
};

module.exports.loginUser = loginUser;
module.exports.addUser = addUser;
module.exports.findUser = findUser;
module.exports.viewFriend = viewFriend;
module.exports.updateLocation = updateLocation;
module.exports.viewProfile = viewProfile;
