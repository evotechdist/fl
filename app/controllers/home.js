var args = arguments[0] || {};
var MapModule = require('ti.map');
var moment = require('alloy/moment');
var indexArray,
    longitude,
    latitude,
    places,
    cal;

var arrLatitude = [];
var arrLongitude = [];
var arrNama = [];
var arrAddress = [];
var arrPhoto = [];
var arrId = [];
var arrDate = [];
var opts = {
    cancel : 2,
    options : ['Ask Location', 'Show Location', 'Cancel'],
    selectedIndex : 2,
    destructive : 0,
    title : 'Option'
};
var dialog = Ti.UI.createOptionDialog(opts);
var collectionFriend = Alloy.Collections.friend;
var service = require('service');

// disable back button android
$.home.addEventListener('android:back', function(e) {
    e.cancelBubble = true;
    Ti.App.fireEvent('expt_back_event');
});
Ti.App.addEventListener('expt_back_event', function(e) {
    Ti.Android.currentActivity.finish();
});

// event listener dialog
dialog.addEventListener('click', function(e) {
    if (e.index === 0) {
        alert('send ?');
    }
    if (e.index === 1) {
        var args = {
            latitude : arrLatitude[indexArray],
            longitude : arrLongitude[indexArray],
            nama : arrNama[indexArray],
            address : arrAddress[indexArray],
            arrLatitude : arrLatitude,
            arrLongitude : arrLongitude,
            arrAddress : arrAddress,
            arrNama : arrNama,
            status : 0
        };
        var map = Alloy.createController('viewmap', args).getView();
        map.open();
    }
});

// loading refresh
function load() {
    Alloy.Globals.loading.show('Refresh..', false);
    setTimeout(function() {
        Alloy.Globals.loading.hide();
    }, 6000);
}

// bindData();
function bindData() {
    var tempName = "robin";
    service.viewFriend(tempName, function(result) {
        if (result.success) {
            if (result.content.status == 1) {
                for (var i = 0; i < result.content.user.length; i++) {
                    var name = result.content.user[i].myFriend;
                    var modelFriend = Alloy.createModel('friend', {
                        nama : result.content.user[i].myFriend,
                        info : "info",
                        imgPhoto : "camera.png",
                        date : "26-01-2016"
                    });
                    collectionFriend.add(modelFriend);
                }
            } else {
                alert("user not found");
            }
        } else {
            alert("login " + result.err);
        }
    });

}

// get Date
function getDate() {
    cal = moment(new Date()).format('LLL');
}

// getAddress
function getAddress(latitude, longitude) {
    Titanium.Geolocation.reverseGeocoder(latitude, longitude, function(evt) {
        places = evt.places[0].street1;
        var address = places;
        var iduser = 2;
        var date = cal;
        // create http client to update location
        
        service.updateLocation(iduser, address, date, latitude, longitude, function(result) {
        if (result.success) {
            alert("sukses");
        } else {
            alert(result.err);
        }
    });
    });
};

// get index from list view
function indexList(e) {
    indexArray = e.itemIndex;
    dialog.show();
}

//refresh list
function myRefresher(e) {
    // load data
    // top/bottom di setting

    e.hide();
}

// cek location/internet connection
function syncLocation() {
    if (Ti.Network.online) {
        if (Ti.Geolocation.locationServicesEnabled) {
            Titanium.Geolocation.getCurrentPosition(function(e) {
                if (!e.success || e.error) {
                    alert('Could not find the device location');
                    return;
                }
                longitude = e.coords.longitude;
                latitude = e.coords.latitude;
                getDate();
                getAddress(latitude, longitude);
            });
        } else {
            alert('Location was disabled');
        }
    } else {
        alert('No internet connection');
    }
}

// to show all friend to map
function showLocation() {
    var args = {
        latitude : latitude,
        longitude : longitude,
        nama : arrNama[indexArray],
        address : arrAddress[indexArray],
        arrLatitude : arrLatitude,
        arrLongitude : arrLongitude,
        arrAddress : arrAddress,
        arrNama : arrNama,
        status : 1
    };
    var map = Alloy.createController('viewmap', args).getView();
    map.open();
}

function add() {
    var add = Alloy.createController("add").getView();
    add.open();
}

function history() {
    var history = Alloy.createController('history').getView();
    history.open();
}

$.search.addEventListener('return', function(e) {

    //e.value = "";
    //$.search.value = "";
});

$.search.addEventListener('cancel', function(e) {

});
$.search.addEventListener('focus', function(e) {
    $.search.value = "";
});

function logOut() {
    var index = Alloy.createController("index").getView();
    index.open();
}

function profile() {
    var profile = Alloy.createController("profile").getView();
    profile.open();
}
