var args = arguments[0] || {};

var tempNama = "robin";
var service = require('service');
var txtSearch;

function find() {
    service.findUser(txtSearch, function(result) {
        if (result.success) {
            if (result.content.status == 1) {
                var name = result.content.user[0].name;
                $.lblNama.text = name;
                $.lblNext.visible = true;
                $.viewBody.visible = true;
            } else {
                alert("user not found");
            }
        } else {
            alert("login " + result.err);
        }
    });
}

function add() {
    service.addUser(txtSearch, tempNama, function(result) {
        //error undefined??
    });
    var home = Alloy.createController("home").getView();
    home.open();

}

$.search.addEventListener('return', function(e) {
    txtSearch = e.value;
    find();
});
$.search.addEventListener('change', function(e) {
    if ($.search.value == "" || e.value == "") {
        $.viewBody.visible = false;
        $.lblNext.visible = false;
    }
});
