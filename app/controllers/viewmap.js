var args = arguments[0] || {};

var latitude = args.latitude;
var longitude = args.longitude;
var nama = args.nama;
var address = args.address;
var status = args.status;

var arrLatitude = args.arrLatitude;
var arrNama = args.arrNama;
var arrLongitude = args.arrLongitude;
var arrAddress = args.arrAddress;
var MapModule = require('ti.map');
var win = $.win;

if (status == 0) {

    var mapview = MapModule.createView({
        mapType : MapModule.NORMAL_TYPE,
        region : {
            latitude : latitude,
            longitude : longitude,
            latitudeDelta : 0.005, // semakin mendekati 1 semakin zoom out
            longitudeDelta : 0.005
        },
    });
    var pin = MapModule.createAnnotation({
        latitude : latitude,
        longitude : longitude,
        title : nama,
        subtitle : address,
        animate : true,
        pincolor : MapModule.ANNOTATION_RED,
        // image :"/setting.png"
    });
    mapview.addAnnotation(pin);
}
if (status == 1) {

    var mapview = MapModule.createView({
        mapType : MapModule.NORMAL_TYPE,
        region : {
            latitude : latitude,//arrlati[0],
            longitude : longitude,//arrlongi[0],
            latitudeDelta : 0.005, // semakin mendekati 1 semakin zoom out
            longitudeDelta : 0.005
        },
    });
    for (var i = 0; i < arrlati.length; i++) {
        var annotations = [];
        var x = MapModule.createAnnotation({
            latitude : arrLatitude[i],
            longitude : arrLongitude[i],
            title : arrNama[i],
            subtitle : arrAddress[i],
            animate : true,
            pincolor : MapModule.ANNOTATION_RED
        });
        annotations[i] = x;
        mapview.addAnnotation(annotations[i]);
    }
}

win.add(mapview);
win.open();