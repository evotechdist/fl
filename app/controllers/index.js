var latitude,
    longitude,
    places,
    cal;
var moment = require('alloy/moment');
var service = require('service');

// androdi back android listener
$.index.addEventListener('android:back', function(e) {
    e.cancelBubble = true;
    Ti.App.fireEvent('exp_back_event');
});
Ti.App.addEventListener('exp_back_event', function(e) {
    Ti.Android.currentActivity.finish();
});

// update address
function getAddress(latitude, longitude) {
    Titanium.Geolocation.reverseGeocoder(latitude, longitude, function(evt) {
        places = evt.places[0].street1;
    });
    //update data
}

// get latitude,longitude, cek gps on/off
function syncLocation() {
    if (Ti.Network.online) {
        if (Ti.Geolocation.locationServicesEnabled) {
            Titanium.Geolocation.getCurrentPosition(function(e) {
                if (!e.success || e.error) {
                    alert("Could not find the device location");
                }
                longitude = e.coords.longitude;
                latitude = e.coords.latitude;
                getDate();
            });
        } else {
            alert('Location was disabled');
        }
    } else {
        alert('No Internet Connection');
    }
}

// get Date
function getDate() {
    cal = momment(new Date()).format('LLL');
    getAddress(latitude, longitude);
}

// loading
function load() {
    Alloy.Globals.loading.show("Welcome", false);
    setTimeOut(function() {
        Alloy.Globals.loading.hide();
    }, 6000);
}

function changeTxtMail(e) {
    $.lblMail.visible = true;
    $.lblMail.height = 20;
    if (e.value == null || e.value == "") {
        $.lblMail.visible = false;
        $.lblMail.height = 0;
    }
}

function focusTxtMail() {
    $.txtPass.focusable = true;
    $.lblMail.color = "cyan";
}

function focusTxtPass() {
    $.lblPass.color = "cyan";
    $.btnLogin.focusable = true;
}

function changeTxtPass(e) {
    $.lblPass.height = 20;
    $.lblPass.visible = true;
    if (e.value == null || e.value == "") {
        $.lblPass.visible = false;
        $.lblPass.height = 0;
    }
}

function login() {
    service.loginUser($.txtMail.value, $.txtPass.value, function(result) {
        if (result.success) {
            // berarti pakai status biar
            if (result.content.status == 1) {
                alert("sukses");
                var home = Alloy.createController("home").getView();
                home.open();

            } else {
                alert("gagal");
            }
        } else {
            alert("login " + result.err);
        }
    });

}

function signUp() {
    var signUp = Alloy.createController("signup").getView();
    signUp.open();
}

function forgetPass() {
    alert("Hubungin Admin di no 08xx");
}

// $.index.open();
// var signUp = Alloy.createController("signup").getView();
// signUp.open();
// var loading = Alloy.createController("loading").getView();
// loading.open();
// var home = Alloy.createController("home").getView();
// home.open();
var add = Alloy.createController("add").getView();
add.open();
// var profile = Alloy.createController("profile").getView();
// profile.open();
// var history = Alloy.createController("history").getView();
// history.open();
