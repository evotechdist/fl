var args = arguments[0] || {};

var service = require('service');

function view() {
    var iduser = 13;
    service.viewProfile(iduser, function(result) {
        if (result.success) {
            if (result.content.status == 1) {
                var img = result.content.user[0].image;
                var decode = Ti.Utils.base64decode(img);
                    var name = result.content.user[0].name;
                    $.lblNama.text = name;
                    $.imgPhoto.image = decode;
            } else {
                alert("user not found");
            }
        } else {
            alert(JSON.stringify(result.err));
        }
    });
}

function back(){
    Alloy.createController("home").getView().open();
}
